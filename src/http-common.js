import axios from "axios";

export default axios.create({
  baseURL: "http://localhost:80/turorials-be/api",
  headers: {
    "Content-type": "application/json"
  }
});