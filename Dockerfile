FROM nginx:mainline-alpine3.18 as builder

WORKDIR /usr/share/nginx/html/

# Copier les fichiers package.json et package-lock.json pour installer les dépendances
COPY package*.json ./

# Installer les dépendances
RUN apk add --no-cache nodejs npm
RUN npm install

# Copier les fichiers de l'application dans le conteneur
COPY . .
RUN ls -alihs
# Construire l'application Angular
RUN npm run build --prod
RUN ls -alihs

# Utilisez une image NGINX pour servir l'application Angular
FROM nginx:mainline-alpine3.18


# Copier le build de l'application depuis le conteneur de construction vers le répertoire de NGINX
COPY --from=builder /usr/share/nginx/html/build/ /usr/share/nginx/html/

# Exposer le port 80 pour accéder à l'application
EXPOSE 80

# La commande par défaut de NGINX est suffisante pour servir l'application
